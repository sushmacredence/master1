var Express = require('express');
var fs = require('fs');
const os = require('os');
const path = require('path');
const moment = require('moment');
var BodyParser = require('body-parser');
var app = Express();
app.use(BodyParser.json());

app.get("/", function (req, res) {
    res.send("WELCOME!!!");
});

app.post("/uploadData", function (req, res) {
    var date = moment().format("DDMMYYYY");
    var file_name = "data_" + date + "_" + Math.floor(Math.random() * 1000) + 1;
    const filename = path.join(__dirname, `${file_name}.csv`);

    const output = []; // holds all rows of data
    output.push(["name","gender","age"]);
    const data = req.body;
    
    out = "";
    var letters = /^[A-Za-z]+$/;
    for (d of data) {
        if (d.name == "" || d.name == null || d.gender == "" || d.gender == null || d.age == "" || d.age == null) {
            out += "Field should not be empty";
        }
        if (isNaN(d.age)) {
            out += "<br>Invalid age";
        }
        if (!d.name.match(letters)) {
            out += "<br>Invalid name";
        }
        if (!d.gender.match(letters)) {
            out += "<br>" + "Invalid Gender";
        }
    }

    if (out == "") {
        data.forEach((d) => {
            const row = []; // a new array for each row of data
            row.push(d.name);
            row.push(d.gender);
            row.push(d.age);

            output.push(row.join()); // by default, join() uses a ','
        });
        fs.writeFileSync(filename, output.join(os.EOL));
        res.send("File uploaded successfully");
        console.log("File uploaded successfully");

    }
    else {
        res.send(out);
        console.log(out);
    }

});

app.get("/sort", function (req, res) {
    fs.readFile("data_11022020_3151.csv", function (err, datas) {
        if (err) {
            console.log(err);
            throw err;
        }
        data = datas.toString();
        arr= data.split(/\r?\n/);
        res.send(arr);
        console.log(arr);
    });
});

app.get('/sort/:gender', function(req,res){
    fs.readFile("data_11022020_3151.csv", function (err, datas) {
        if (err) {
            console.log(err);
            throw err;
        }
        let filter_gen = [];
        let gen_val = req.params.gender;

        let str = datas.toString();

        data = str.split(/\r?\n/);
        
        data.forEach((ele) =>{
            let row = ele.split(",");
           if(gen_val.toLocaleLowerCase()=="female" || gen_val.toLocaleLowerCase() == "f"){
                if(row.includes("female") || row.includes("f")) {
                    filter_gen.push(row);
                }
            }
            if(gen_val.toLocaleLowerCase()=="male" || gen_val.toLocaleLowerCase() == "m"){
                if(row.includes("male") || row.includes("m")){
                   filter_gen.push(row);
                }
            }
        }); //end of foreach

        console.log(filter_gen);
        out_str="name,gender,age<br />";
        for (x of filter_gen){
            out_str += x[0]+","+x[1]+","+x[2]+"<br />";
        }
        res.send(out_str);
    }); //end of readFile
}); 

app.listen(3001, () => {
    console.log("Server is listening to port 3001....");
});